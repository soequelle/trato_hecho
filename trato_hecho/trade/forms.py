from django import forms

from trade.models import TradeItem, TradeOffer

class TradeItemForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'picture', 'description', 'tags', 'tradeable_for')
        model = TradeItem

class TradeOfferForm(forms.ModelForm):
    class Meta:
        exclude = ('target', 'status', 'created_at', 'updated_at',)
        model = TradeOffer
    '''
    def clean(self):
        cleaned_data = self.cleaned_data.copy()
        target = cleaned_data.get('target')
        offered = cleaned_data.get('offered')
        if target.owner == offered.owner:
            raise forms.ValidationError("Target and offerring user is the same.")
        return cleaned_data
    '''
