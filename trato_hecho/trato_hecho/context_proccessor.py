from django.conf import settings

def facebook_settings(request):
    facebook_settings = {
        'namespace': settings.FACEBOOK_APPLICATION_NAMESPACE,
        'app_id': settings.FACEBOOK_APPLICATION_ID
        }
    return  {'facebook_settings':facebook_settings}
